import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from "../pages/home/home";
import { SummaryPage } from "../pages/summary/summary";
import { InActionPage } from '../pages/in-action/in-action';
import { FriendsPage } from "../pages/friends/friends";
import { AuthProvider } from "../providers/auth";
import { WelcomePage } from "../pages/welcome/welcome";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'SummaryPage';
  @ViewChild(Nav) nav: Nav;


  // used for an example of ngFor and navigation
  pages = [
    { title: 'Start', component: HomePage, icon: "home" },
    { title: 'Tracking', component: InActionPage, icon: "speedometer" },
    { title: 'Friends', component: FriendsPage, icon: "people" },
    { title: 'Maps', component: SummaryPage, icon: "list" },
    { title: 'Logout', action: this.logout.bind(this), icon: "log-out" }
  ];

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    private auth: AuthProvider,
    splashScreen: SplashScreen
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  logout() {
    console.log('Logging out...');
    this.auth.authorize().signOut()
      .then(() => {
        console.log('Logged out');
        this.nav.setRoot(WelcomePage);
      });
  }

  openPage(page) {
    if (page.action) {
      return page.action();
    }

    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

