import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from "../home/home";
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AuthProvider } from '../../providers/auth';
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage implements OnInit {
  nick: string = '';
  private basePath: string = '/users';
  dbItem$: Observable<any>;
  authChecked: boolean;

  constructor(
    private auth: AuthProvider,
    private nav: NavController,
    private db: AngularFireDatabase
  ) {
  }

  ngOnInit() {
    console.log('LoginPage ngOnInit');
    this.auth.authState().subscribe(auth => {
      console.log('WelcomePage auth', auth);
      if (!auth) {
        this.authChecked = true;
        return;
      }
      auth.getToken().then((token: string) => {
        if (token) {
          this.navigate();
        }
      });
    });
  }

  private anonymous() {
    if (!this.nick) {
      return alert(`You didn't type your nick!`);
    }

    this.auth.authorize().signInAnonymously()
      .then((val) => {
        console.log('Auth val', val);
        this.db.object(this.basePath + `/${val.uid}`).set({ name: this.nick });
        if (!this.authChecked) {
          this.navigate();
        }
      });
  }

  private navigate() {
    this.nav.setRoot(HomePage);
  }



}
