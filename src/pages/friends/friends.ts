import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {AddFriendPage} from "../add-friend/add-friend";
import {AngularFireDatabase} from 'angularfire2/database';
import {Vibration} from '@ionic-native/vibration';

interface Friend {
  name: string;
}

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {
  coordinates: any;
  friends = [];


  constructor(private navCtrl: NavController,
              private db: AngularFireDatabase,
              private vibration: Vibration) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendsPage');
    this.listFriends();
  }

  addFriend() {
    this.navCtrl.push(AddFriendPage);
  }

  getWeightByDanger(danger) {
    if (danger === 'yes')
      return 1;
    if (danger === 'maybe')
      return 2;
    if (danger === 'no')
      return 3;
    return 4;
  }

  listFriends() {
    this.db.list('/users')
      .valueChanges()
      .subscribe(friends => {
        console.log(friends);
        this.friends = friends.map(friend => ({
          ...friend,
          sortIndex: this.getWeightByDanger(friend['danger'])
        }));

        this.friends.sort((a, b) => a.sortIndex - b.sortIndex);

        this.friends.forEach(friend => {
          for (let key in friend) {
            if (key === 'danger' && friend[key] === 'yes') {
              this.friendindanger();
            }
          }
        })
      });
  }

  friendindanger() {
    this.vibration.vibrate([500, 100, 500, 100, 500, 100, 500]);
  }


}
