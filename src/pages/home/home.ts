import { Component } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";
import { Geolocation } from '@ionic-native/geolocation';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';
import { ModalController } from 'ionic-angular';
import { InActionPage } from '../in-action/in-action';
import { AuthProvider } from '../../providers/auth';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  locationId: any;
  motion: any;
  motionData: any = [];
  dista: any = '';

  constructor(
    private auth: AuthProvider,
    private geolocation: Geolocation,
    private db: AngularFireDatabase,
    private deviceMotion: DeviceMotion,
    private modalCtrl: ModalController
  ) {
  }

  startModal() {
    let modal = this.modalCtrl.create(InActionPage);
    modal.onDidDismiss(() => this.stop());
    modal.present();
  }

  start() {
    console.log('start');
    this.auth.authState().subscribe(val => {
      this.db.object(`users/${val.uid}`).update({
        tracking: true
      })
        .then(() => this.logCoordinates());
    });

    this.motionStart();
    this.startModal();
  }

  stop() {
    this.stopGeolocation();
    this.auth.authState().subscribe(val => {
      this.db.object(`users/${val.uid}`).update({
        tracking: false,
        danger: 'no'
      })
    });
    this.motionStop();
    console.log('stop');
  }

  stopGeolocation() {
    this.locationId.unsubscribe();
  }

  logCoordinates() {
    this.locationId = this.geolocation.watchPosition({ enableHighAccuracy: true })
      .subscribe(position => {
        console.log(position);
        this.sendCoorinates(position);
      });
  }

  private sendCoorinates(position) {
    this.auth.authState().subscribe(val => {
      this.db.list(`locations/`).push({
        uid: val.uid,
        lat: position.coords.latitude,
        lng: position.coords.longitude,
        at: position.timestamp
      });
    });

  }

  countDistance(v1: any, v2: any) {
    const xl = v2.x - v1.x;
    const yl = v2.y - v1.y;
    const zl = v2.z - v1.z;
    return Math.sqrt(xl ** 2 + yl ** 2 + zl ** 2);
  }

  motionStart() {
    let last: any;
    const prog = 40;
    this.motion = this.deviceMotion.watchAcceleration({ frequency: 250 }).subscribe((acceleration: DeviceMotionAccelerationData) => {
      if (last) {
        const distance = this.countDistance(last, acceleration);
        this.dista = 'OK';
        if (distance >= prog) {
          this.dista = 'BAD';
          this.auth.authState().subscribe(val => {
            this.db.object('users/' + val.uid).update({ danger: 'maybe' });
          });
        }
      }
      last = acceleration;
    });
  }

  motionStop() {
    this.motion.unsubscribe();
  }

}
