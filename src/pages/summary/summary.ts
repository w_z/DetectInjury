import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import mapboxgl from 'mapbox-gl';
const MAP_TOKEN = 'pk.eyJ1Ijoic2FqdGVtcGxlciIsImEiOiJjajljZ3RubG0xcGt0MnFtcWMzc3F5eXZ5In0.cS_oaboaeN5xmb9zMyv5-Q';


@IonicPage()
@Component({
  selector: 'page-summary',
  templateUrl: 'summary.html',
})
export class SummaryPage {
  map: mapboxgl.Map;
  myLocation: number[] = [91.9987, 31.5279];

  constructor() {
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  loadMap() {
    mapboxgl.accessToken = MAP_TOKEN;
    this.map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v9',
      center: this.myLocation,
      zoom: 8
    });
    this.addMeToMap();
  }

  addMeToMap() {
    let geojson = {
      "type": "FeatureCollection",
      "features": [
        {
          "type": "Feature",
          "properties": {
            "message": "Here",
            "iconSize": [60, 60]
          },
          "geometry": {
            "type": "Point",
            "coordinates": this.myLocation
          }
        }
      ]
    };

    geojson.features.forEach((marker) => {
      // create a DOM element for the marker
      var el = document.createElement('div');
      el.className = 'marker';
      el.style.backgroundImage = 'url(http://s.funny.pho.to/fc732f3-004/images/sample-preview-boy.jpg)';
      el.style.width = marker.properties.iconSize[0] + 'px';
      el.style.height = marker.properties.iconSize[1] + 'px';

      el.addEventListener('click', () => {
        window.alert(marker.properties.message);
      });

      // add marker to map
      new mapboxgl.Marker(el)
        .setLngLat(marker.geometry.coordinates)
        .addTo(this.map);
    });
  }

  private createLngLat(lng: number, lat: number): mapboxgl.LngLat {
    return new mapboxgl.LngLat(lng, lat);
  }

  delete() {
    console.log('deleting');
  }
}
