import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FIREBASE_CONFIG } from './env';
import { AngularFireModule } from 'angularfire2';
import { WelcomePageModule } from '../pages/welcome/welcome.module';
import { InActionPageModule } from '../pages/in-action/in-action.module';
import { AngularFireAuth } from 'angularfire2/auth';
import { EstimoteBeacons } from '@ionic-native/estimote-beacons';
import { SummaryPageModule } from "../pages/summary/summary.module";
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { FriendsPageModule } from "../pages/friends/friends.module";
import { AddFriendPageModule } from "../pages/add-friend/add-friend.module";
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from "@ionic-native/google-maps";
import { DeviceMotion } from '@ionic-native/device-motion';
import { Vibration } from '@ionic-native/vibration';
import { AuthProvider } from '../providers/auth';


// import { FriendsPage } from './friends';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule,
    IonicModule.forRoot(MyApp),
    WelcomePageModule,
    InActionPageModule,
    FriendsPageModule,
    SummaryPageModule,
    AddFriendPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AngularFireAuth,
    EstimoteBeacons,
    AngularFireDatabase,
    Geolocation,
    GoogleMaps,
    DeviceMotion,
    Vibration,
    AuthProvider
  ]
})
export class AppModule { }
